<?php

declare(strict_types=1);

namespace Drupal\kameleoon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Configure Kameleoon settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'kameleoon_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['kameleoon.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Kameleoon'),
      '#description' => $this->t('Check this box to enable Kameleoon on this site.'),
      '#default_value' => $this->config('kameleoon.settings')->get('enable'),
    ];
    // The project sitecode can be configured only if it is not set in the
    // settings.
    if ($this->sitecodeIsInSettings()) {
      $form['sitecode'] = [
        '#type' => 'item',
        '#title' => $this->t('Project sitecode'),
        '#markup' => $this->t('The project sitecode is already set in the settings.'),
      ];
    }
    else {
      $form['sitecode'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Project sitecode'),
        '#description' => $this->t('The project sitecode provided by <a href="https://kameleoon.com">Kameleoon</a>.'),
        '#default_value' => $this->config('kameleoon.settings')->get('sitecode'),
      ];
    }
    $form['loading_timeout'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 100,
      '#title' => $this->t('Loading timeout'),
      '#description' => $this->t('Duration in milliseconds to wait while the Kameleoon application file is loaded.'),
      '#default_value' => $this->config('kameleoon.settings')->get('loading_timeout'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // If Kameleoon is enabled, the project sitecode is required.
    if (
      !$this->sitecodeIsInSettings() &&
      $form_state->getValue('enable') &&
      empty($form_state->getValue('sitecode'))
    ) {
      $form_state->setErrorByName(
        'sitecode',
        $this->t('The project sitecode is required when Kameleoon is enabled.'),
      );
    }
    // Validate, if the loading timeout is a positive integer.
    if ($form_state->getValue('loading_timeout') < 0) {
      $form_state->setErrorByName(
        'loading_timeout',
        $this->t('The loading timeout must be a positive integer.'),
      );
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('kameleoon.settings')
      ->set('enable', $form_state->getValue('enable'))
      ->set('sitecode', $form_state->getValue('sitecode'))
      ->set('loading_timeout', $form_state->getValue('loading_timeout'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Tests if the sitecode is set in the settings.
   *
   * @return bool
   *   TRUE if the sitecode is set, FALSE otherwise.
   */
  protected function sitecodeIsInSettings(): bool {
    if (
      ($settings = Settings::get('kameleoon')) &&
      array_key_exists('sitecode', $settings) &&
      !empty($settings['sitecode'])
    ) {
      return TRUE;
    }
    return FALSE;
  }

}
