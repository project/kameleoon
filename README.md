## INTRODUCTION

This module provides integration with [Kameleoon](https://www.kameleoon.com) A/B
testing solution.

## REQUIREMENTS

The Kameleoon module has no requirements.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

The Kameleoon module configuration page can be found at:
`/admin/config/services/kameleoon`

The Kameleoon project sitecode can be also set in your settings.php file:

```php
$settings['kameleoon']['sitecode'] = 'your_kameleoon_project_sitecode';
```

## MAINTAINERS

Current maintainers for Drupal 10/11:

- [Antonín Slejška](https://www.drupal.org/u/anton%C3%ADn-slej%C5%A1ka)
