/**
 * @file Kameleoon load.
 */

(function (Drupal, drupalSettings, once) {
  Drupal.behaviors.kameleoonBehavior = {
    attach(context, settings) {
      once('kameleoonBehavior', 'html', context).forEach(function (element) {
        // Duration in milliseconds to wait while the Kameleoon application file is loaded
        const kameleoonLoadingTimeout =
          drupalSettings.kameleoon.loadingTimeout || 1000;
        window.kameleoonQueue = window.kameleoonQueue || [];
        window.kameleoonStartLoadTime = new Date().getTime();
        if (
          !document.getElementById('kameleoonLoadingStyleSheet') &&
          !window.kameleoonDisplayPageTimeOut
        ) {
          const kameleoonS = document.getElementsByTagName('script')[0];
          const kameleoonCc =
            '* { visibility: hidden !important; background-image: none !important; }';
          const kameleoonStn = document.createElement('style');
          kameleoonStn.type = 'text/css';
          kameleoonStn.id = 'kameleoonLoadingStyleSheet';
          if (kameleoonStn.styleSheet) {
            kameleoonStn.styleSheet.cssText = kameleoonCc;
          } else {
            kameleoonStn.appendChild(document.createTextNode(kameleoonCc));
          }
          kameleoonS.parentNode.insertBefore(kameleoonStn, kameleoonS);
          window.kameleoonDisplayPage = function (fromEngine) {
            if (!fromEngine) {
              window.kameleoonTimeout = true;
            }
            if (kameleoonStn.parentNode) {
              kameleoonStn.parentNode.removeChild(kameleoonStn);
            }
          };
          window.kameleoonDisplayPageTimeOut = window.setTimeout(
            window.kameleoonDisplayPage,
            kameleoonLoadingTimeout,
          );
        }
      });
    },
  };
})(Drupal, drupalSettings, once);
